from flask import Flask
from src.settings import logger
from src.common import database
from flasgger import Swagger
from werkzeug.middleware.proxy_fix import ProxyFix

# app
app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
    SECRET_KEY="dev",
    SQLALCHEMY_DATABASE_URI=f"sqlite:///fison.sqlite",
)
swagger = Swagger(app)


def init_db():
    database.db.init_app(app)
    with app.app_context():
        logger.info("Run schema.sql")
        database.db.create_all()


def init_marshmallow():
    from src.io_schemas.responses.general_response import ma

    ma.init_app(app)


def register_blueprints():
    logger.info("Register Blueprint")
    # api must import after db.init_app()
    from src.api_flask import product
    from src.api_flask import category

    app.register_blueprint(product.bp)
    app.register_blueprint(category.bp)


def register_error_handlers():
    from src.common.utils import error_handlers, exceptions
    from pydantic import ValidationError
    from sqlalchemy.exc import IntegrityError, NoResultFound

    app.register_error_handler(Exception, error_handlers.handle_exception)
    app.register_error_handler(exceptions.InvalidData, error_handlers.handle_400)
    app.register_error_handler(ValidationError, error_handlers.handle_validator_error)
    app.register_error_handler(IntegrityError, error_handlers.handle_duplication)
    app.register_error_handler(NoResultFound, error_handlers.handle_no_result_found)


def init_app():
    app.wsgi_app = ProxyFix(
        app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
    )
    init_db()
    init_marshmallow()
    register_blueprints()
    register_error_handlers()


@app.route("/")
def home():
    return "Hello World2"


@app.teardown_appcontext
def shutdown_session(exception=None):
    if exception and database.db.session.is_active:
        database.db.session.rollback()
    database.db.session.remove()


init_app()


if __name__ == "__main__":
    app.run(
        debug=True,
    )
