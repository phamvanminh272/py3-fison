import json
from json.decoder import JSONDecodeError
from src.settings import logger
from logging import Logger
from pydantic import ValidationError


def load_request_args(request_args: dict):
    """Load request args to dictionary"""
    for k, v in request_args.items():
        try:
            if v:
                request_args[k] = json.loads(v)
        except JSONDecodeError:
            pass
    return request_args
