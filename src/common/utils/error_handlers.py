from flask import json, make_response
from src.settings import logger


def handle_exception(e):
    logger.exception(e)
    response = error_response("Internal server error", 500)
    return response


def handle_400(e):
    logger.exception(e)
    return error_response(str(e), 400)


def handle_validator_error(e):
    """Pydantic error"""
    logger.exception(e)
    msg = ".".join(str(i) for i in e.errors()[0]["loc"]) + ": " + e.errors()[0]["msg"]
    return error_response(msg, 400)


def handle_duplication(e):
    logger.exception(e)
    return error_response("Duplication error", 400)


def handle_no_result_found(e):
    logger.exception(e)
    return error_response("Object not found", 404)


def error_response(msg, status_code):
    return make_response({"msg": msg}, status_code)
