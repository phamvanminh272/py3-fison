from sqlalchemy import asc, desc, String, or_
from dataclasses import dataclass
from pydantic import BaseModel
from typing import Any, TypeVar, cast, TYPE_CHECKING
from functools import lru_cache
from src.common.utils.exceptions import InvalidData
from src.io_schemas.requests.general_objects import (
    OrderBy,
    FilterBy,
    Pagination,
    SearchRequestModel,
)
from typing import List


direction = {"asc": asc, "desc": desc}


@dataclass(frozen=True)
class _GetFields:
    _model: type[BaseModel]

    def __getattr__(self, item: str) -> Any:
        if item in self._model.model_fields:
            return item

        return getattr(self._model, item)


TModel = TypeVar("TModel", bound=type[BaseModel])


def fields(model: TModel, /) -> TModel:
    return cast(TModel, _GetFields(model))


if not TYPE_CHECKING:
    fields = lru_cache(maxsize=256)(fields)


def search_text_builder(stmt, search_text, search_text_catalog):
    if search_text:
        search_str = f"%{search_text}%"
        conditions = [col.ilike(search_str) for col in search_text_catalog]
        stmt = stmt.filter(or_(*conditions))
    return stmt


def filter_builder(stmt, filter_by: List[FilterBy], filter_catalog: dict):
    for i in filter_by:
        column_name = i.name
        value = i.value
        if column_name in filter_catalog.keys():
            column_db = filter_catalog[column_name]
            col_type = column_db.expression.type
            if isinstance(col_type, String):
                stmt = stmt.filter(column_db.ilike(f"%{value}%"))
            else:
                stmt = stmt.filter(column_db == value)

    return stmt


def order_builder(stmt, order_by: OrderBy, order_catalog: dict):
    if order_by:
        order_column = order_catalog.get(order_by.name)
        if not order_column:
            raise InvalidData(f"Not support order by {order_by.name} yet")
        stmt = stmt.order_by(direction.get(order_by.direction)(order_column))
    return stmt


def pagination_builder(stmt, pagination: Pagination):
    if pagination:
        stmt = stmt.offset((pagination.page - 1) * pagination.limit).limit(
            pagination.limit
        )
    return stmt


def query_builder(
    stmt,
    search_model: SearchRequestModel,
    search_text_catalog,
    filter_catalog,
    order_catalog,
):
    if search_model:
        stmt = search_text_builder(stmt, search_model.searchText, search_text_catalog)
        stmt = filter_builder(stmt, search_model.filterBy, filter_catalog)
        stmt = order_builder(stmt, search_model.orderBy, order_catalog)
    return stmt
