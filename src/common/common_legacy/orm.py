from typing import List
from sqlalchemy import (
    Column,
    String,
    Integer,
    Numeric,
    create_engine,
    Boolean,
    Text,
    ForeignKey,
)
from sqlalchemy.orm import (
    declarative_base,
    sessionmaker,
    Mapped,
    mapped_column,
    relationship,
)


Base = declarative_base()

conn_str = "postgresql+psycopg2://postgres:12345678@fison.ctmjkvo11d39.us-west-2.rds.amazonaws.com:5432/fison"


class DBSession(object):
    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.engine = create_engine(conn_str)
            cls.session = sessionmaker(cls.engine)()
            cls.instance = super(DBSession, cls).__new__(cls)
        return cls.instance


DBSession()


class CategoriesTable(Base):
    __tablename__ = "categories"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(100))
    super_category_name: Mapped[str] = mapped_column(String(50))


class ProductsTable(Base):
    __tablename__ = "products"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(100))
    description: Mapped[str] = mapped_column(Text)
    sum_quantity: Mapped[int] = mapped_column()
    code: Mapped[str] = mapped_column(String(10))
    instances: Mapped[List["InstancesTable"]] = relationship(back_populates="product")


class ProductCategoryTable(Base):
    __tablename__ = "product_category"
    product_id: Mapped[int] = mapped_column(primary_key=True)
    category_id: Mapped[int] = mapped_column(primary_key=True)


class AttributesTable(Base):
    __tablename__ = "attributes"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50))
    value: Mapped[str] = mapped_column(String(50))

    instances: Mapped["InstanceAttributeTable"] = relationship(
        back_populates="attribute"
    )


class InstancesTable(Base):
    __tablename__ = "instances"
    id: Mapped[int] = mapped_column(primary_key=True)
    product_id: Mapped[int] = mapped_column(ForeignKey("products.id"))
    quantity: Mapped[int] = mapped_column()
    price: Mapped[float] = mapped_column()
    pictures: Mapped[str] = mapped_column(Text)
    code: Mapped[str] = mapped_column(String(50))
    default_display: Mapped[bool] = mapped_column(default=False)

    product: Mapped[ProductsTable] = relationship(back_populates="instances")
    attributes: Mapped[List["InstanceAttributeTable"]] = relationship(
        back_populates="instance"
    )


class InstanceAttributeTable(Base):
    __tablename__ = "instance_attribute"
    instance_id: Mapped[int] = mapped_column(
        ForeignKey("instances.id"), primary_key=True
    )
    attribute_id: Mapped[int] = mapped_column(
        ForeignKey("attributes.id"), primary_key=True
    )
    value: Mapped[str] = mapped_column(String(50))

    instance: Mapped[InstancesTable] = relationship(back_populates="attributes")
    attribute: Mapped[AttributesTable] = relationship(back_populates="instances")
