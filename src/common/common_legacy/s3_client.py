import boto3

from settings import AWS_CREDENTIALS, BUCKET_NAME, BUCKET_URL, logger


class S3Client:
    def __init__(self):
        session = boto3.Session(
            aws_access_key_id=AWS_CREDENTIALS.get("aws_access_key_id"),
            aws_secret_access_key=AWS_CREDENTIALS.get("aws_secret_access_key"),
        )
        self.s3_resource = session.resource("s3")
        self.client = session.client("s3")
        self.bucket = self.s3_resource.Bucket(BUCKET_NAME)
        self.pictures_folder = "pictures/"

    def get_file_names(self, path):
        names = []
        for i in self.bucket.objects.filter(Prefix=path):
            names.append(f"{BUCKET_URL}/{i.key}")
        return ";".join(names[1:])

    def create_folder(self, folder_name):
        self.client.put_object(
            Bucket=BUCKET_NAME, Key=(self.pictures_folder + folder_name + "/")
        )
