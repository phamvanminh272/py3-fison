from sqlalchemy import select, update, and_

from settings import logger
from common.orm import InstancesTable, ProductsTable
from common.s3_client import S3Client


class Instances:
    def __init__(self, session):
        self.session = session

    def update_pictures(self, ins_id=None, pro_id=None):
        logger.info("Updating pictures ...")
        ins_id_filter = InstancesTable.id == ins_id if ins_id else 1 == 1
        pro_id_filter = ProductsTable.id == pro_id if pro_id else 1 == 1
        stmt = select(
            InstancesTable.id,
            InstancesTable.pictures,
            InstancesTable.code.label("ins_code"),
            ProductsTable.code.label("pro_code"),
        ).where(
            and_(
                InstancesTable.product_id == ProductsTable.id,
                ins_id_filter,
                pro_id_filter,
            )
        )
        product_intances = self.session.execute(stmt)
        s3 = S3Client()
        for i in product_intances:
            pictures = s3.get_file_names(f"pictures/{i.pro_code}/{i.ins_code}/")
            if pictures != i.pictures:
                stmt = (
                    update(InstancesTable)
                    .where(InstancesTable.id == i.id)
                    .values(pictures=pictures)
                )
                self.session.execute(stmt)
                logger.info(f"Updated pictures for product instance {i.id}")
        self.session.commit()
        logger.info("Updating done.")
