POST_PRODUCT = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "description": {"type": "string"},
        "category_ids": {"type": "array"},
    },
    "required": ["name", "category_ids"],
}

POST_INSTANCE = {
    "type": "object",
    "properties": {
        "product_id": {"type": "number"},
        "pictures": {"type": "string"},
        "price": {"type": "number"},
        "quantity": {"type": "number"},
        "default_display": {"type": "boolean"},
    },
    "required": ["product_id", "pictures", "price", "quantity", "default_display"],
}
