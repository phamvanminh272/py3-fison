import logging
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from common.exceptions import InvalidData


def validate_input(data, schema):
    try:
        validate(data, schema)
    except ValidationError as e:
        logging.exception(e)
        raise InvalidData(e.message)
