from sqlalchemy import select

from common.orm import CategoriesTable, ProductCategoryTable
from common.exceptions import InvalidData
import contants


class Categories:
    def __init__(self, session):
        self.session = session

    def select(self):
        categories = self.session.query(CategoriesTable).all()
        categories_dict = {}
        for i in categories:
            if i.super_category_name in categories_dict.keys():
                categories_dict[i.super_category_name].append(
                    {"id": i.id, "name": i.name}
                )
            else:
                categories_dict[i.super_category_name] = [{"id": i.id, "name": i.name}]
        self.session.close()
        categories_list = []
        for i, v in categories_dict.items():
            categories_list.append({i: v})
        return categories_list

    def insert(self, data):
        if data.get("super_category_name") not in contants.SUPER_CATEGORY_NAME:
            raise InvalidData(
                f"super_category_name should be in {', '.join(contants.SUPER_CATEGORY_NAME)}"
            )
        stmt = select([CategoriesTable]).where(CategoriesTable.name == data.get("name"))
        category = self.session.scalars(stmt).one_or_none()
        if category:
            raise InvalidData(f"{data.get('name')} already exists")
        category = CategoriesTable(
            name=data.get("name"),
            super_category_name=data.get("super_category_name"),
        )
        self.session.add(category)
        self.session.commit()

    def update(self, data):
        stmt = select([CategoriesTable]).where(CategoriesTable.id == data.get("id"))
        category = self.session.scalars(stmt).one_or_none()
        if not category:
            raise InvalidData(f"Category id {data.get('id')} not found")
        category.name = data.get("name")
        self.session.commit()

    def delete(self, category_id):
        stmt = select([CategoriesTable]).where(CategoriesTable.id == category_id)
        category = self.session.scalars(stmt).one_or_none()
        if not category:
            raise InvalidData(f"Category id {category_id} not found")
        stmt = select([ProductCategoryTable.product_id]).where(
            ProductCategoryTable.category_id == category_id
        )
        product = self.session.scalars(stmt).all()
        if product:
            raise InvalidData(f"There are products in this category")
        self.session.delete(category)
        self.session.commit()
