class SerializedType(object):
    def __init__(self, _type):
        self._type = _type


class String(SerializedType):
    def __init__(self):
        super(String, self).__init__(str)


class Integer(SerializedType):
    def __init__(self):
        super(Integer, self).__init__(int)


class Boolean(SerializedType):
    def __init__(self):
        super(Boolean, self).__init__(bool)


class Array(SerializedType):
    def __init__(self):
        super(Array, self).__init__(list)


class Serializer(object):
    def __init__(self):
        self._fields = {
            name: f for name, f in self.__class__.__dict__.items() if "__" not in name
        }

    def serialize(self, obj, many=False):
        if many:
            ls = []
            for each in obj:
                data = self._serialize(each)
                ls.append(data)
            return ls
        else:
            return self._serialize(obj)

    def _serialize(self, obj):
        d = {}
        for name, field in self._fields.items():
            v = obj.__getattr__(name)
            if not v:
                value = v
            else:
                value = v if field._type == v.__class__ else field._type(v)
            d.update({name: value})
        return d
