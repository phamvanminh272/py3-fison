from sqlalchemy import select
from sqlalchemy.orm import Session

from src.common.database import Product
from src.io_schemas.requests.product import (
    ProductSearchRequestModelModel,
    ProductRequestModel,
)
from src.common.utils.exceptions import InvalidData
from src.common.utils.data_provider_utils import direction, fields


class ProductDP:
    def __init__(self, session: Session):
        self.session = session

    def search(self, search_model: ProductSearchRequestModelModel):
        stmt = select(Product.id, Product.name).select_from(Product)

        order_by = {
            fields(ProductRequestModel).id: Product.id,
            fields(ProductRequestModel).name: Product.name,
        }
        order_column = order_by.get(search_model.orderBy.name)
        if not order_column:
            raise InvalidData("")
        stmt = stmt.order_by(
            direction.get(search_model.orderBy.direction)(order_column)
        )

        data = self.session.execute(stmt).all()
        response = [{"id": i.id, "name": i.name} for i in data]
        return response
