from sqlalchemy import select, cast, String, func
from sqlalchemy.orm import Session

from src.common.database import Category
from src.io_schemas.requests.category import (
    CategoryRequestModel,
    CategorySearchRequestModel,
)
from src.common.utils.data_provider_utils import (
    fields,
    query_builder,
    pagination_builder,
)
from src.settings import logger


class CategoryDP:
    def __init__(self, session: Session):
        self.session = session

    def search(self, search_model: CategorySearchRequestModel = None):
        logger.info("Starting query db ..")
        stmt = select(Category.id, Category.name, Category.description)
        stmt_count = select(func.count(Category.id))

        def build_joining(stmt_select):
            return stmt_select.select_from(Category)

        stmt = build_joining(stmt)
        stmt_count = build_joining(stmt_count)

        search_text_catalog = [
            cast(Category.id, String),
            Category.name,
        ]

        filter_catalog = {
            fields(CategoryRequestModel).id: Category.id,
            fields(CategoryRequestModel).name: Category.name,
            fields(CategoryRequestModel).description: Category.description,
        }

        order_catalog = {
            fields(CategoryRequestModel).id: Category.id,
            fields(CategoryRequestModel).name: Category.name,
            fields(CategoryRequestModel).description: Category.description,
        }

        stmt = query_builder(
            stmt, search_model, search_text_catalog, filter_catalog, order_catalog
        )
        stmt_count = query_builder(
            stmt_count, search_model, search_text_catalog, filter_catalog, order_catalog
        )

        total_row = self.session.execute(stmt_count).scalar()
        stmt = pagination_builder(stmt, search_model.pagination)
        data = self.session.execute(stmt).all()
        return data, total_row

    def get_by_id(self, category_id):
        logger.info(f"Starting query Category id {category_id} ...")
        stmt = (
            select(Category.id, Category.name, Category.description)
            .select_from(Category)
            .filter(Category.id == category_id)
        )
        data = self.session.execute(stmt).one()
        return data

    def add(self, category: CategoryRequestModel):
        obj = Category(
            name=category.name,
            description=category.description,
            category_id=category.category_id,
        )

        self.session.add(obj)
        return obj
