from fastapi import APIRouter

router = APIRouter(prefix="/api/v1/product")

products_tag = "products"


@router.get("", tags=[products_tag])
def search_products():
    print("")
    return [{"username": "Rick"}, {"username": "Morty"}]

