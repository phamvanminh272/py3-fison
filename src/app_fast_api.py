from fastapi import FastAPI
import uvicorn

app = FastAPI()

def include_routers():
    from src.api_fastapi.v1.product import router
    app.include_router(router)


def init_app():
    include_routers()

if __name__ == "__main__":
    init_app()
    uvicorn.run(app, host="127.0.0.1", port=8000)
