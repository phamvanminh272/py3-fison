from typing import List

from pydantic import BaseModel, field_validator, Field

from src.common.utils.exceptions import InvalidData
from src.io_schemas.requests.general_objects import (
    FilterBy,
    OrderBy,
    SearchRequestModel,
)


class CategoryRequestModel(BaseModel):
    id: int | None = None
    name: str = Field(min_length=1)
    description: str
    category_id: int


class CategoryFilterByModel(FilterBy):
    @field_validator("name")
    def validate_name(cls, v):
        allowed_values = list(CategoryRequestModel.model_fields.keys())
        if v not in CategoryRequestModel.model_fields.keys():
            raise InvalidData(f"filterBy.name: '{v}' should be in {allowed_values}")
        return v


class CategoryOrderByModel(OrderBy):
    @field_validator("name")
    def validate_name(cls, v):
        allowed_values = list(CategoryRequestModel.model_fields.keys())
        if v not in CategoryRequestModel.model_fields.keys():
            raise InvalidData(f"orderBy.name: '{v}' should be in {allowed_values}")
        return v


class CategorySearchRequestModel(SearchRequestModel):
    filterBy: List[CategoryFilterByModel] = Field(default=[])
    orderBy: CategoryOrderByModel = Field(default=None)
