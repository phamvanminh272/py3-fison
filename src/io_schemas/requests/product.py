from typing import List

from pydantic import BaseModel, field_validator

from src.common.utils.exceptions import InvalidData
from src.io_schemas.requests.general_objects import (
    FilterBy,
    OrderBy,
    SearchRequestModel,
)


class ProductRequestModel(BaseModel):
    id: int
    name: str


class ProductSearchFilterBy(FilterBy):
    @field_validator("name")
    def validate_name(cls, v):
        allowed_values = list(ProductRequestModel.model_fields.keys())
        if v not in ProductRequestModel.model_fields.keys():
            raise InvalidData(f"filterBy.name: '{v}' should be in {allowed_values}")
        return v


class ProductSearchOrderBy(OrderBy):
    @field_validator("name")
    def validate_name(cls, v):
        allowed_values = list(ProductRequestModel.model_fields.keys())
        if v not in ProductRequestModel.model_fields.keys():
            raise InvalidData(f"orderBy.name: '{v}' should be in {allowed_values}")
        return v


class ProductSearchRequestModelModel(SearchRequestModel):
    filterBy: List[ProductSearchFilterBy]
    orderBy: ProductSearchOrderBy
