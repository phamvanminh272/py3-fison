from typing import Literal, List
from pydantic import BaseModel, Field


class FilterBy(BaseModel):
    name: str
    value: str | int | list


class OrderBy(BaseModel):
    name: str
    direction: Literal["asc", "desc"]


class Pagination(BaseModel):
    page: int = Field(default=1, gt=0)
    limit: int = Field(default=20)


class SearchRequestModel(BaseModel):
    searchText: str | None = Field(default=None)
    filterBy: List[FilterBy]
    orderBy: OrderBy
    pagination: Pagination = Field(default=Pagination())
