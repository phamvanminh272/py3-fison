from typing import List

# from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow

ma = Marshmallow()


class SearchResponseMaSchema(ma.Schema):
    class Meta:
        fields = (
            "hasPrev",
            "hasNext",
            "page",
            "totalPage",
            "limit",
            "totalItems",
            "data",
            "_links",
        )

    _links = ma.Hyperlinks(
        {
            "self": ma.URLFor("api_v1_categories.list_categories"),
            "prev": ma.URLFor("api_v1_categories.list_categories"),
            "next": ma.URLFor("api_v1_categories.list_categories"),
        }
    )


class CategoryMaSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ("id", "name", "description", "links")

    # Smart hyperlinking
    _links = ma.Hyperlinks(
        {
            "self": ma.URLFor(
                "api_v1_categories.get_categories", values=dict(category_id="<id>")
            ),
            "collection": ma.URLFor("api_v1_categories.list_categories"),
        }
    )
