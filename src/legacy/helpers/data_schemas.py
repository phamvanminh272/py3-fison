POST_CATEGORY = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "super_category_name": {"type": "string"},
    },
    "required": ["name", "super_category_name"],
}

PUT_CATEGORY = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "id": {"type": "number"},
    },
    "required": ["name", "id"],
}
