from common.orm import ProductsTable, ProductCategoryTable, InstancesTable
from sqlalchemy import select, and_, func


class InstanceObj:
    def __init__(self, session, data=None):
        self.session = session
        self.raw_data = data
        self.product_obj = None

    # def parse_raw_data(self):

    def insert(self):
        ins_obj = InstancesTable(
            product_id=self.raw_data["product_id"],
            pictures=self.raw_data["pictures"],
            quantity=self.raw_data["quantity"],
            price=self.raw_data["price"],
            default_display=self.raw_data["default_display"],
            code=self.raw_data["code"],
        )
        self.session.add(ins_obj)
        self.session.flush()
        for i in self.raw_data["attr"]:
            ins_attr = ProductCategoryTable(instance_id=ins_obj.id, attribute_id=i)
            self.session.add(ins_attr)
            self.session.flush()
        self.session.commit()
