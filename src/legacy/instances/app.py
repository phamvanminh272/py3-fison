import json

from instances.instance import InstanceObj
from common.json_schemas import POST_INSTANCE
from common.utils import validate_input
from common.orm import DBSession


def lambda_handler(event, context):
    session = DBSession().session
    try:
        if event["httpMethod"] == "POST":
            data = json.loads(event["body"])
            ins_id = InstanceObj(data).insert()

            return {
                "statusCode": 200,
                "body": json.dumps({"data": ins_id}, ensure_ascii=False),
            }
    finally:
        session.close()


if __name__ == "__main__":
    # event = {
    #     "httpMethod": "GET",
    #     # "queryStringParameters": {"category_id": 3},
    #     "pathParameters": {"id": 1},
    # }
    # result = lambda_handler(event, "")
    # print(result)

    event = {
        "httpMethod": "POST",
        "body": {
            "name": "T-shirt test 1",
            "description": "",
            "code": "SP33",
            "category_ids": [1, 2],
        },
    }
    results = lambda_handler(event, "")
