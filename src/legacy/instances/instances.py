from common.orm import InstanceAttributeTable, InstancesTable
from sqlalchemy import select, and_, func
from common.s3_client import S3Client


class InstanceObj:
    def __init__(self, session, data=None):
        self.session = session
        self.raw_data = data
        self.instance_obj = None

    def insert(self):
        attrs = self.raw_data["attrs"]
        code = ""
        for k in sorted(attrs.keys()):
            code += str(attrs[k])

        instance_obj = InstancesTable(
            product_id=self.raw_data["product_id"],
            quantity=self.raw_data["quantity"],
            price=self.raw_data["price"],
            code=code,
            default_display=self.raw_data["price"],
        )
        self.session.add(instance_obj)
        self.session.flush()
        for i in attrs.keys():
            ins_attr = InstanceAttributeTable(
                instance_id=instance_obj.id, attribute_id=i
            )
            self.session.add(ins_attr)
            self.session.flush()
        self.session.commit()
        return instance_obj.id
