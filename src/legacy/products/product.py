from common.orm import ProductsTable, ProductCategoryTable, InstancesTable
from sqlalchemy import select, and_, func
from common.s3_client import S3Client


class ProductObj:
    def __init__(self, session, data=None):
        self.session = session
        self.raw_data = data
        self.product_obj = None
        self.code_prefix = "SP"

    # def parse_raw_data(self):

    def insert(self):
        product_obj = ProductsTable(
            name=self.raw_data["name"],
            description=self.raw_data.get("description"),
            sum_quantity=0,
            code="",
        )
        self.session.add(product_obj)
        self.session.flush()
        # update code
        product_obj.code = self.code_prefix + str(product_obj.id)
        # update product category
        for i in self.raw_data["category_ids"]:
            product_category = ProductCategoryTable(
                product_id=product_obj.id, category_id=i
            )
            self.session.add(product_category)
        self.session.commit()
        s3 = S3Client().create_folder(product_obj.code)
        return product_obj.id

    def get_product_details(self, product_id):
        stmt = select(ProductsTable).where(ProductsTable.id == product_id)
        product = self.session.scalars(stmt).first()
        instances = []
        attrs_dict = {}
        if not product.instances:
            product.price = 0
            product.pictures = []
        for i in product.instances:
            pictures = i.pictures or ""
            instances.append(
                {
                    "id": i.id,
                    "price": i.price,
                    "pictures": pictures.split(";"),
                    "quantity": i.quantity,
                    "code": i.code,
                }
            )
            if i.default_display:
                product.price = i.price
                product.pictures = pictures.split(";")
            for attr in i.attributes:
                if not attrs_dict.get(attr.attribute.name):
                    attrs_dict.update({attr.attribute.name: [attr.value]})
                else:
                    if attr.value not in attrs_dict[attr.attribute.name]:
                        attrs_dict[attr.attribute.name].append(attr.value)

        product_dict = {
            "id": product.id,
            "name": product.name,
            "description": product.description,
            "sum_quantity": product.sum_quantity,
            "code": product.code,
            "price": product.price,
            "pictures": product.pictures,
            "instances": instances,
            "attrs": attrs_dict,
        }

        return product_dict

    def list(self, category_id, page=1, size=10):
        join_category = (
            ProductCategoryTable.product_id == ProductsTable.id
            if category_id
            else 1 == 1
        )
        category_id_filter = (
            ProductCategoryTable.category_id == category_id if category_id else 1 == 1
        )
        offset = (page - 1) * size
        statement = (
            select(
                ProductsTable.id,
                ProductsTable.name,
                InstancesTable.price,
                func.string_to_array(InstancesTable.pictures, ";").label("pictures"),
            )
            .where(
                and_(
                    InstancesTable.default_display == True,
                    ProductsTable.id == InstancesTable.product_id,
                    join_category,
                    category_id_filter,
                )
            )
            .offset(offset)
            .limit(size)
        )
        rs = self.session.execute(statement)
        return rs
