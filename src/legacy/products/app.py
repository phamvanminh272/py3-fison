import json

from settings import logger
from common.orm import DBSession
from common.utils import validate_input
from common.json_schemas import POST_PRODUCT
from products.product import ProductObj
from products.serializers import ProductSerializer
from common.exceptions import InvalidData


def lambda_handler(event, context):
    session = DBSession().session
    params = event.get("queryStringParameters") or {}
    page = int(params.get("page", 1))
    size = int(params.get("size", 10))
    category_id = int(params.get("category_id", 0))
    try:
        if event["httpMethod"] == "GET":
            if event.get("pathParameters"):
                product_id = int(event.get("pathParameters").get("id"))
                product = ProductObj(session).get_product_details(product_id)
                return {
                    "statusCode": 200,
                    "body": json.dumps(product, ensure_ascii=False),
                }
            else:
                products = ProductObj(session).search(category_id, page, size)
                rs = ProductSerializer().serialize(products, many=True)

                return {
                    "statusCode": 200,
                    "body": json.dumps(rs, ensure_ascii=False),
                }
        elif event["httpMethod"] == "POST":
            data = json.loads(event["body"])
            validate_input(data, POST_PRODUCT)
            product_id = ProductObj(session, data).insert()
            return {"statusCode": 200, "body": json.dumps({"data": product_id})}
    except InvalidData as e:
        logger.exception(e)
        return {"statusCode": 200, "body": json.dumps({"message": str(e)})}
    finally:
        session.close()


if __name__ == "__main__":
    event = {
        "httpMethod": "GET",
        # "queryStringParameters": {"category_id": 3},
        "pathParameters": {"id": 11},
    }
    # result = lambda_handler(event, "")
    # print(result)

    event_insert = {
        "httpMethod": "POST",
        "body": json.dumps({"name": "T-shirt test 1", "category_ids": [1, 2]}),
    }
    results = lambda_handler(event, "")
    results = json.loads(results["body"])
    results = json.dumps(results, indent=4)
    print(results)
