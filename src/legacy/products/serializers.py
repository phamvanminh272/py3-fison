from common.serializers import Serializer, String, Integer, Array


class ProductSerializer(Serializer):
    id = String()
    name = String()
    price = Integer()
    pictures = Array()
