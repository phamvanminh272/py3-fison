"""
Populate Attributes table locally
"""

from sqlalchemy.orm import Session

from common.orm import AttributesTable, DBSession

data = {
    "Size": [
        "29",
        "30",
        "31",
        "32",
        "33",
        "34",
        "35",
        "S",
        "M",
        "L",
        "XL",
        "2XL",
        "3XL",
    ],
    "Màu Sắc": [
        "Xanh Lá",
        "Đỏ",
        "Tím",
        "Vàng",
        "Hồng",
        "Nâu",
        "Đen",
        "Trắng",
        "Xanh Dương",
    ],
}


def populate():
    with Session(DBSession().engine) as session:
        for i in data.keys():
            v = ";".join(data[i])
            attr_db = (
                session.query(AttributesTable)
                .filter(AttributesTable.name == i)
                .one_or_none()
            )
            if attr_db:
                print(f"Update {i}")
                attr_db.value = v
            else:
                print(f"Insert {i}")
                attr = AttributesTable(name=i, value=v)
                session.add(attr)
        session.commit()


if __name__ == "__main__":
    populate()
