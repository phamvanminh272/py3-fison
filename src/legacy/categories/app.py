import json
import logging

from common.orm import DBSession
from common.categories import Categories
from common.exceptions import InvalidData
from common.utils import validate_input
from helpers.data_schemas import POST_CATEGORY, PUT_CATEGORY


def lambda_handler(event, context):
    try:
        session = DBSession().session
        categories = Categories(session)
        if event["httpMethod"] == "GET":
            categories = categories.select()
            return {
                "statusCode": 200,
                "body": json.dumps(categories, ensure_ascii=False),
            }
        elif event["httpMethod"] == "POST":
            data = json.loads(event["body"])
            validate_input(data, POST_CATEGORY)
            categories.insert(data)
            return {
                "statusCode": 201,
                "body": json.dumps("Successfully", ensure_ascii=False),
            }
        elif event["httpMethod"] == "PUT":
            data = json.loads(event["body"])
            validate_input(data, PUT_CATEGORY)
            categories.update(data)
            return {
                "statusCode": 201,
                "body": json.dumps("Successfully", ensure_ascii=False),
            }
        elif event["httpMethod"] == "DELETE":
            category_id = event["pathParameters"].get("id")
            try:
                category_id = int(category_id)
            except ValueError as e:
                logging.error(e)
                raise InvalidData(f"id {category_id} aws not an integer")
            categories.delete(category_id)
            return {
                "statusCode": 204,
                "body": json.dumps("Successfully", ensure_ascii=False),
            }
    except InvalidData as e:
        logging.exception(e)
        return {
            "statusCode": 400,
            "body": json.dumps({"message": str(e)}, ensure_ascii=False),
        }


if __name__ == "__main__":
    event = {"httpMethod": "GET", "pathParameters": {"id": 4}}
    result = lambda_handler(event, "")
    print(result)
