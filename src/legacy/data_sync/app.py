import json

from settings import logger
from common.orm import DBSession
from common.instances import Instances


def lambda_handler(event, context):
    session = DBSession().session
    try:
        ins = Instances(session)
        if event["httpMethod"] == "PUT":
            ins_id = None
            pro_id = None
            params = event["queryStringParameters"]
            if params:
                ins_id = int(params.get("instance_id") or 0)
                pro_id = int(params.get("product_id") or 0)
            ins.update_pictures(ins_id, pro_id)
            logger.info("Update successfully")
            return {
                "statusCode": 200,
                "body": json.dumps("Successfully", ensure_ascii=False),
            }
        return {
            "statusCode": 400,
            "body": json.dumps("Please use http PUT method", ensure_ascii=False),
        }
    except TypeError as ex:
        logger.error(
            f'instance_id or product_id {event["queryStringParameters"]} are not an integer'
        )
        return {
            "statusCode": 400,
            "body": json.dumps(
                f'instance_id or product_id {event["queryStringParameters"]} are not an integer',
                ensure_ascii=False,
            ),
        }
    finally:
        session.close()


if __name__ == "__main__":
    # event = {"httpMethod": "PUT", "queryStringParameters": None}
    event = {
        "httpMethod": "PUT",
        "queryStringParameters": {"instance_id": 3, "product_id": None},
    }
    result = lambda_handler(event, "")
    print(result)
