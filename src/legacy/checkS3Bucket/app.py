import json

import boto3


def lambda_handler(event, context):
    params = event["queryStringParameters"]
    if not params:
        return {
            "statusCode": 404,
            "body": json.dumps(
                {"message": "Please provide file-path"}, ensure_ascii=False
            ),
        }
    file_path = params.get("file-path")
    print(f"Start check file {file_path}")
    if not file_path:
        return {
            "statusCode": 404,
            "body": json.dumps(
                {"message": "Please provide file-path"}, ensure_ascii=False
            ),
        }
    s3 = boto3.client("s3", region_name="us-west-2")
    demo_bucket_name = "www.fison.store"

    try:
        rs = s3.head_object(Bucket=demo_bucket_name, Key=file_path)
        print(rs)
        print("Key exists !!!")
    except s3.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "404":
            print("Key does not exist !!!")
            return {
                "statusCode": 200,
                "body": json.dumps({"fileFound": False}, ensure_ascii=False),
            }
        else:
            # Handle Any other type of error
            raise
    return {
        "statusCode": 200,
        "body": json.dumps({"fileFound": True}, ensure_ascii=False),
    }


if __name__ == "__main__":
    event = ""
    result = lambda_handler(event, "")
    print(result)
