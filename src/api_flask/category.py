from src.settings import logger, API_VERSION

from flask import request, make_response
from flask.blueprints import Blueprint

from src.common.database import db
from src.common.utils.api_utils import load_request_args
from src.io_schemas.requests.category import (
    CategoryRequestModel,
    CategorySearchRequestModel,
)
from src.io_schemas.responses.general_response import (
    CategoryMaSchema,
    SearchResponseMaSchema,
)
from src.services.category_service import CategoryService
from flasgger import swag_from

bp = Blueprint(f"api_{API_VERSION}_categories", __name__, url_prefix=f"/api/{API_VERSION}/categories")


@bp.route("/", methods=["GET"])
@swag_from({
    'parameters': [
        {
            'name': 'category_id',
            'in': 'path',
            'type': 'integer',
            'required': True,
            'description': 'The ID of the category'
        }
    ]
})
def list_categories():
    logger.info("Getting categories ...")
    request_args = load_request_args(request.args.to_dict())
    request_model = CategorySearchRequestModel(**request_args)

    data = CategoryService(db.session).search(request_model)

    data["data"] = CategoryMaSchema(many=True).dump(data["data"])
    response = SearchResponseMaSchema().dump(data)

    return response


@bp.route("/<category_id>", methods=["GET"])
def get_categories(category_id: int):
    data = CategoryService(db.session).get_by_id(category_id)
    response = CategoryMaSchema().dump(data)
    return response


@bp.route("/", methods=["POST"])
def add_category():
    request_model = CategoryRequestModel(**request.json)
    data = CategoryService(db.session).add(request_model)
    response = CategoryMaSchema().dump(data)
    return make_response(response, 201)
