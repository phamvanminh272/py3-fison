from flask import request
from flask.blueprints import Blueprint
from src.common.database import db

from src.common.data_providers.product import ProductDP
from src.io_schemas.requests.product import ProductSearchRequestModelModel
from src.settings import logger, API_VERSION

bp = Blueprint(f"api_{API_VERSION}_products", __name__, url_prefix=f"/api/{API_VERSION}/products")


@bp.route("/search", methods=["POST"])
def list_products():
    request_data = ProductSearchRequestModelModel(**request.json)
    data = ProductDP(db.session).search(request_data)
    return {"data": data}


# @bp.route("/", methods=["POST"])
# def add_product():
#     connection = get_db()
#     cur = connection.cursor()
#     cur.execute("Insert into product(`name`) values('Minh');")
#     connection.commit()
#     return {"msg": "Successfully"}
