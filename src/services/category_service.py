import math
from sqlalchemy.orm import Session
from src.io_schemas.requests.category import (
    CategoryRequestModel,
    CategorySearchRequestModel,
)
from src.common.data_providers.category import CategoryDP
from src.common.utils.data_provider_utils import fields


class CategoryService:
    def __init__(self, session: Session):
        self.session = session

    def search(self, category_model: CategorySearchRequestModel = None):
        data, total_row = CategoryDP(self.session).search(category_model)

        items = [
            {
                fields(CategoryRequestModel).id: i.id,
                fields(CategoryRequestModel).name: i.name,
                fields(CategoryRequestModel).description: i.description,
            }
            for i in data
        ]

        response = {
            "data": items,
            "hasPrev": category_model.pagination.page > 1,
            "hasNext": category_model.pagination.page < math.ceil(total_row/category_model.pagination.limit),
            "page": category_model.pagination.page,
            "limit": category_model.pagination.limit,
            "totalItems": total_row,
        }
        return response

    def get_by_id(self, category_id):
        data = CategoryDP(self.session).get_by_id(category_id)
        response = {
            fields(CategoryRequestModel).id: data.id,
            fields(CategoryRequestModel).name: data.name,
            fields(CategoryRequestModel).description: data.description,
        }
        return response

    def add(self, category: CategoryRequestModel):
        obj = CategoryDP(self.session).add(category)
        self.session.commit()
        response = {
            fields(CategoryRequestModel).id: obj.id,
            fields(CategoryRequestModel).name: obj.name,
            fields(CategoryRequestModel).description: obj.description,
        }
        return response
