--CREATE TABLE if not exists 'user' (
--  id INTEGER PRIMARY KEY AUTOINCREMENT,
--  username TEXT UNIQUE NOT NULL,
--  password TEXT NOT NULL
--);
--
--CREATE TABLE if not exists 'post' (
--  id INTEGER PRIMARY KEY AUTOINCREMENT,
--  author_id INTEGER NOT NULL,
--  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
--  title TEXT NOT NULL,
--  body TEXT NOT NULL,
--  FOREIGN KEY (author_id) REFERENCES user (id)
--);

CREATE TABLE if not exists 'product' (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT UNIQUE NOT NULL,
  description TEXT,
  category_id INTEGER,
  FOREIGN KEY(category_id) REFERENCES product_category(id)
);


CREATE TABLE if not exists 'product_category' (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT UNIQUE NOT NULL,
  description TEXT,
  category_id INTEGER
);

